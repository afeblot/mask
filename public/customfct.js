Mavo.Functions.max_date = function(all_dates) {
    var max_dt = all_dates[0];
    var max_dtObj = new Date(all_dates[0]);
    all_dates.forEach(function(dt, index) {
        if ( new Date( dt ) > max_dtObj) {
            max_dt = dt;
            max_dtObj = new Date(dt);
        }
      });
    return max_dt;
}

Mavo.Functions.add_days = function(date, days) {
    const dtObj = new Date(date);
    dtObj.setDate(dtObj.getDate() + days);
    return dtObj.toISOString().slice(0,10)
}

Number.prototype.pad = function(size) {
    var sign = Math.sign(this) === -1 ? '-' : '';
    return sign + new Array(size).concat([Math.abs(this)]).join('0').slice(-size);
}

Mavo.Functions.human_duration = function(minutes) {
    var sign = Math.sign(minutes) === -1 ? '-' : '';
    minutes = Math.abs(minutes)
    var h = Math.floor(minutes/60);
    var m = minutes % 60;
    var out = sign
    if (h>0) {
        out += h + "h";
    }
    if (m>0 || h==0) {
        out += m.pad(2);
    }
    if (h==0) {
        out += " min"
    }
    return out;
}
